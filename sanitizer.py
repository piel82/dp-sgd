# the sanitizer is based on the implementation of the TensorFlow Authors

# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================


import collections
import tensorflow as tf

import utility

clip_option = collections.namedtuple("clip_option", ["l2_norm_bound", "clip"])


class Sanitizer(object):
    """
    Sanitize a tensor with gaussian noise and
    keep track of privacy spending via accountant
    """

    def __init__(self, accountant, default_clip_option):
        self.accountant_ = accountant
        self.default_clip_option_ = default_clip_option
        self.options_ = {}
    

    def set_option(self, tensor_name, option):
        self.options_[tensor_name] = option
    
    def sanitize(self, x, eps_delta, sigma=None, option=clip_option(None, None),
                    tensor_name=None, num_examples=None, add_noise=True):
        """
        sanitize the tensor x by applying the l2 norm clipping and then adding gaussian noise
        """

        if sigma is None:
            eps, delta = eps_delta
            # formula for sigma found at "the algorithmic foundations of differential privacy", appendix a: http://www.cis.upenn.edu/~aaroth/Papers/privacybook.pdf
            sigma = tf.sqrt(2. * tf.math.log(1.25 / delta)) / eps
        
        l2_norm_bound, clip = option
        if l2_norm_bound is None:
            l2_norm_bound, clip = self.default_clip_option_
            if tensor_name is not None and tensor_name in self.options_:
                l2_norm_bound, clip = self.options_[tensor_name]
        
        if clip:
            x = tf.clip_by_norm(x, clip_norm=l2_norm_bound)
        
        if add_noise:
            if num_examples is None:
                num_examples = tf.slice(tf.shape(x), [0], [1])
            privacy_accumulation_operator = self.accountant_.accumulate_privacy_spending(eps_delta, sigma, num_examples)
            with tf.control_dependencies([privacy_accumulation_operator]):
                sanitized_x = utility.add_noise(x, sigma * l2_norm_bound)
        else:
            sanitized_x = tf.reduce_sum(x, 0)
        
        return sanitized_x
