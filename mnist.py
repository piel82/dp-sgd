import numpy as np
import tensorflow as tf
import time
import matplotlib.pyplot as plt
import collections
from accountant import *
from sanitizer import *
from pathlib import Path
from sklearn.model_selection import train_test_split
import random
import os

# Set various seeds to 0 to get reproducible results
os.environ['PYTHONHASHSEED'] = str(0)
random.seed(0)
np.random.seed(0)

root_dir = Path(__file__).absolute().parent
models_dir = Path.joinpath(root_dir, "models")
results_dir = Path.joinpath(root_dir, "results")

EpsDelta = collections.namedtuple("EpsDelta", ["spent_eps", "spent_delta"])
batch_size = 64
lr = 5e-2
l2_norm_bound = 4.
sigma = 4.
dataset = "mnist" if len(sys.argv) < 2 else sys.argv[1]
model_type = "dense"
use_privacy = False if len(sys.argv) >= 3 and sys.argv[2] == "False" else True
plot_results = True
epochs = 260

def load_mnist(image_size=28):
    train, test = tf.keras.datasets.mnist.load_data()
    train_data, train_labels = train
    test_data, test_labels = test

    train_data = np.array(train_data, dtype=np.float32) / 255
    test_data = np.array(test_data, dtype=np.float32) / 255

    train_data = train_data.reshape(train_data.shape[0], 28, 28, 1)
    test_data = test_data.reshape(test_data.shape[0], 28, 28, 1)

    train_labels = np.array(train_labels, dtype=np.int32)
    test_labels = np.array(test_labels, dtype=np.int32)

    train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=10)
    test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)

    return train_data, train_labels, test_data, test_labels


def load_fashion_mnist(image_size=28):
    train, test = tf.keras.datasets.fashion_mnist.load_data()
    train_data, train_labels = train
    test_data, test_labels = test

    train_data = np.array(train_data, dtype=np.float32) / 255
    test_data = np.array(test_data, dtype=np.float32) / 255

    train_data = train_data.reshape(train_data.shape[0], 28, 28, 1)
    test_data = test_data.reshape(test_data.shape[0], 28, 28, 1)

    train_labels = np.array(train_labels, dtype=np.int32)
    test_labels = np.array(test_labels, dtype=np.int32)

    train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=10)
    test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)

    return train_data, train_labels, test_data, test_labels


# dense model for mnist dataset
def get_dense_model(input_shape, units, num_classes):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=input_shape, dtype="float64"))
    model.add(tf.keras.layers.Dense(units, activation="relu", dtype="float64"))
    model.add(tf.keras.layers.Dense(num_classes, activation="softmax", dtype="float64"))
    return model


# cnn model for fashion_mnist dataset
def get_cnn_model(input_shape, num_classes):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform',
                                    input_shape=input_shape))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(64, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dense(num_classes, activation='softmax'))
    return model


def get_random_batch(X, y, batch_size=64):
    take = np.random.randint(0, len(X) - 1, size=batch_size)
    return X[take], y[take]


def main():
    if dataset == "mnist":
        X_train, y_train, X_test, y_test = load_mnist()
        num_classes = 10
        image_size = 28
        n_channels = 1
    else:
        X_train, y_train, X_test, y_test = load_fashion_mnist()
        num_classes = 10
        image_size = 28
        n_channels = 1
    
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.2, random_state=0)

    model = None
    if model_type == "dense":
        model = get_dense_model((image_size, image_size, n_channels), image_size * image_size, num_classes)
    else:
        model = get_cnn_model((image_size, image_size, n_channels), num_classes)
    
    loss_func = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
    optimizer = tf.optimizers.SGD(lr)

    eps = 1.
    delta = 1e-5
    max_eps = 64.
    max_delta = 1e-3
    target_eps = [64.]
    target_deltas = [1e-5]
    eps_final = None
    delta_final = None

    # initialize metrics
    train_acc_scores = []
    valid_acc_scores = []
    train_loss_scores = []
    valid_loss_scores = []
    train_mean_loss = tf.keras.metrics.Mean()
    valid_mean_loss = tf.keras.metrics.Mean()
    train_acc_metric = tf.keras.metrics.CategoricalAccuracy()
    valid_acc_metric = tf.keras.metrics.CategoricalAccuracy()
    train_metrics = [tf.keras.metrics.CategoricalAccuracy()]
    valid_metrics = [tf.keras.metrics.CategoricalAccuracy()]
    test_metrics = [tf.keras.metrics.CategoricalAccuracy()]

    # initialize accountant and sanitizer for differential privacy
    accountant = GaussianMomentsAccountant(len(X_train))
    sanitizer = Sanitizer(accountant, [l2_norm_bound / batch_size, True])

    start_time = time.time()
    spent_eps_delta = EpsDelta(0, 0)
    terminate = False
    n_steps = len(X_train) // batch_size
    version = "dp_sgd" if use_privacy else "sgd"


    # training loop
    for epoch in range(1, epochs + 1):
        if terminate:
            spent_eps = spent_eps_delta.spent_eps
            spent_delta = spent_eps_delta.spend_delta
            print(f"Algorithm is ({spent_eps}, {spent_delta})-differentially private. Halting now.")
            break

        print(f"Epoch: {epoch}/{epochs}")
        for step in range(1, n_steps + 1):
            X_batch, y_batch = get_random_batch(X_train, y_train)
            with tf.GradientTape() as tape:
                y_pred = model(X_batch, training=True)
                main_loss = tf.reduce_mean(loss_func(y_batch, y_pred))
                loss = tf.add_n([main_loss] + model.losses)
                train_acc_metric.update_state(y_batch, y_pred)
            gradients = tape.gradient(loss, model.trainable_variables)

            if use_privacy:
                sanitized_grads = []
                eps_delta = EpsDelta(eps, delta)
                for px_grad in gradients:
                    sanitized_grad = sanitizer.sanitize(px_grad, eps_delta, sigma)
                    sanitized_grads.append(np.array(sanitized_grad, dtype=np.float64))
                spent_eps_delta_arr = accountant.get_privacy_spent(target_deltas=target_deltas)
                spent_eps_delta = spent_eps_delta_arr[0]
                eps_final, delta_final = spent_eps_delta
                optimizer.apply_gradients(zip(sanitized_grads, model.trainable_variables))
                if spent_eps_delta.spent_eps > max_eps or spent_eps_delta.spent_delta > max_delta:
                    terminate = True
            else:
                optimizer.apply_gradients(zip(gradients, model.trainable_variables))
            
            train_mean_loss(loss)
            for metric in train_metrics:
                metric(y_batch, y_pred)
            
            if step % 200 == 0:
                time_taken = time.time() - start_time
                for metric in valid_metrics:
                    X_batch, y_batch = get_random_batch(X_valid, y_valid)
                    y_pred = model(X_batch, training=False)
                    main_loss = tf.reduce_mean(loss_func(y_batch, y_pred))
                    loss = tf.add_n([main_loss] + model.losses)
                    valid_mean_loss(loss)
                    valid_acc_metric.update_state(y_batch, y_pred)
                    metric(y_batch, y_pred)
                
                if use_privacy:
                    print_status_bar(step * batch_size, len(y_train), train_mean_loss, time_taken, train_metrics + valid_metrics, spent_eps_delta_arr)
                else:
                    print_status_bar(step * batch_size, len(y_train), train_mean_loss, time_taken, train_metrics + valid_metrics)

            if terminate:
                break

        train_acc = train_acc_metric.result()
        train_loss = train_mean_loss.result()
        train_acc_scores.append(train_acc)
        train_loss_scores.append(train_loss)
        train_acc_metric.reset_states()
        train_mean_loss.reset_states()

        valid_acc = valid_acc_metric.result()
        valid_loss = valid_mean_loss.result()
        valid_acc_scores.append(valid_acc)
        valid_loss_scores.append(valid_loss)
        valid_acc_metric.reset_states()
        valid_mean_loss.reset_states()

        if epoch % 10 == 0:
            for metric in test_metrics:
                y_pred = model(X_test, training=False)
                metric(y_test, y_pred)
            metrics = " - ".join(["{}: {:.4f}".format(m.name, m.result())
                                for m in test_metrics or []])
            print(f"Epoch {epoch} test accuracy: {metrics}")
        
        if epoch > 0 and epoch % 50 == 0:
            eps_delta_str = f"-{eps_final}-{delta_final}" if eps_final != None else ""
            filename = f"{version}-{epoch}-{model_type}-{dataset}" + eps_delta_str + ".h5"
            model.save(models_dir/filename)


    for metric in test_metrics:
        y_pred = model(X_test, training=False)
        metric(y_test, y_pred)
    metrics = " - ".join(["{}: {:.4f}".format(m.name, m.result())
                        for m in test_metrics or []])
    print(f"Training completed, test metrics: {metrics}")
    eps_delta_str = f"-{eps_final}-{delta_final}" if eps_final != None else ""
    filename = f"{version}-{epochs}-{model_type}-{dataset}" + eps_delta_str + ".h5"
    model.save(models_dir/filename)

    if plot_results:
        epochs_range = range(1, epochs + 1)
        plt.figure(figsize=(8,6))
        plt.plot(epochs_range, train_loss_scores, color='blue', label='Training loss')
        plt.plot(epochs_range, valid_loss_scores, color='red', label='Validation loss')
        plt.title('Training and validation loss')
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.legend()
        eps_delta_str = f"-{eps_final}-{delta_final}" if eps_final != None else ""
        filename = f"{version}-Loss-{epochs}-{model_type}-{dataset}" + eps_delta_str + ".png"
        plt.savefig(results_dir/filename)
        plt.close()
        
        plt.figure(figsize=(8,6))
        plt.plot(epochs_range, train_acc_scores, color='blue', label='Training accuracy')
        plt.plot(epochs_range, valid_acc_scores, color='red', label='Validation accuracy')
        plt.title('Training and validation accuracy')
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.legend()
        eps_delta_str = f"-{eps_final}-{delta_final}" if eps_final != None else ""
        filename = f"{version}-Accuracy-{epochs}-{model_type}-{dataset}" + eps_delta_str + ".png"
        plt.savefig(results_dir/filename)
        plt.close()



def print_status_bar(iteration, total, loss, time_taken, metrics=None, spent_eps_delta_arr=None):
    metrics = " - ".join(["{}: {:.4f}".format(m.name, m.result())
                        for m in [loss] + (metrics or [])])
    end = "" if iteration < total else "\n"
    if spent_eps_delta_arr:
        print("\r{}/{} - ".format(iteration, total) + metrics + " - time spent: " + f"{time_taken}" "\n", end=end)
        print("eps_delta combinations:")
        for spent_eps_delta in spent_eps_delta_arr:
            print(f"eps = {spent_eps_delta.spent_eps}, delta = {spent_eps_delta.spent_delta}\n")

    else:
        print("\r{}/{} - ".format(iteration, total) + metrics + " - spent eps: " +
            " - time spent: " + f"{time_taken}" "\n", end=end)


if __name__ == "__main__":
    main()
