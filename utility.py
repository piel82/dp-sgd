import numpy as np
import tensorflow as tf
import math


def gen_binomial_table(sz):
    """
    Generate binomial table B of size "sz" where B[n][k] = (n choose k)
    """

    B = np.zeros((sz + 1, sz + 1), dtype=np.float64)
    for i in range(sz + 1):
        B[i, 0] = 1
    
    for i in range(1, sz + 1):
        for j in range(1, sz + 1):
            B[i, j] = B[i - 1, j] + B[i - 1, j - 1]
    
    return tf.convert_to_tensor(B)


def add_noise(x, sigma, name=None):
    """
    add gaussian noise (0, sigma^2) to every entry of the tensor x
    and return the noisy tensor
    """
    x = tf.cast(x, np.float64)
    return x + tf.random.normal(tf.shape(x), stddev=sigma, dtype=np.float64)