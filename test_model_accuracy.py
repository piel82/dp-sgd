import numpy as np
import tensorflow as tf
import collections
from pathlib import Path
from sklearn.model_selection import train_test_split
import random
import os
import sys

# Set various seeds to 0 to get reproducible results
os.environ['PYTHONHASHSEED'] = str(0)
random.seed(0)
np.random.seed(0)

root_dir = Path(__file__).absolute().parent
models_dir = Path.joinpath(root_dir, "models")
results_dir = Path.joinpath(root_dir, "results")

EpsDelta = collections.namedtuple("EpsDelta", ["spent_eps", "spent_delta"])
batch_size = 64
lr = 5e-2
l2_norm_bound = 4.
sigma = 4.
dataset = "mnist" if len(sys.argv) < 2 else sys.argv[1]
model_type = "dense"
use_privacy = True
plot_results = True
epochs = 256

def load_mnist(image_size=28):
    train, test = tf.keras.datasets.mnist.load_data()
    train_data, train_labels = train
    test_data, test_labels = test

    train_data = np.array(train_data, dtype=np.float32) / 255
    test_data = np.array(test_data, dtype=np.float32) / 255

    train_data = train_data.reshape(train_data.shape[0], 28, 28, 1)
    test_data = test_data.reshape(test_data.shape[0], 28, 28, 1)

    train_labels = np.array(train_labels, dtype=np.int32)
    test_labels = np.array(test_labels, dtype=np.int32)

    train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=10)
    test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)

    return train_data, train_labels, test_data, test_labels


def load_fashion_mnist(image_size=28):
    train, test = tf.keras.datasets.fashion_mnist.load_data()
    train_data, train_labels = train
    test_data, test_labels = test

    train_data = np.array(train_data, dtype=np.float32) / 255
    test_data = np.array(test_data, dtype=np.float32) / 255

    train_data = train_data.reshape(train_data.shape[0], 28, 28, 1)
    test_data = test_data.reshape(test_data.shape[0], 28, 28, 1)

    train_labels = np.array(train_labels, dtype=np.int32)
    test_labels = np.array(test_labels, dtype=np.int32)

    train_labels = tf.keras.utils.to_categorical(train_labels, num_classes=10)
    test_labels = tf.keras.utils.to_categorical(test_labels, num_classes=10)

    return train_data, train_labels, test_data, test_labels



def main():
    if dataset == "mnist":
        X_train, y_train, X_test, y_test = load_mnist()
    else:
        X_train, y_train, X_test, y_test = load_fashion_mnist()
    
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.2, random_state=0)

    if len(sys.argv) < 3:
        print("Too few command line arguments. Enter the dataset as first argument and the model file name from the model in the models directory")
        sys.exit(1)

    load_model = sys.argv[2]
    model = tf.keras.models.load_model(models_dir/load_model)
    
    
    test_metrics = [tf.keras.metrics.CategoricalAccuracy()]

    for metric in test_metrics:
        y_pred = model(X_test, training=False)
        metric(y_test, y_pred)
    metrics = " - ".join(["{}: {:.4f}".format(m.name, m.result())
                        for m in test_metrics or []])
    print(f"Testing completed, test metrics: {metrics}")

if __name__ == "__main__":
    main()
